INSERT INTO users(
            id, nric_name, phone_number, status, email, encrypted_password, created_at, updated_at)
    VALUES 
    (7, 'Rinnie Lee Siew Mui', '6596640563', 0, 'mycsgop@gmail.com', '$2y$10$T05jRtHwATc4B63yUnAri.CVzMweU/GWRAwwvElXgqFZJR.zyfbBu', '2016-01-15 17:48:53', '2016-05-15 11:32:49'),
	(10, 'Chua Wan Chee', '91718334', 0, 'wanghee0310@gmail.com', '$2y$10$23mRNJAyPj/cL/7IYdhaTeIFHZ0haO.ubDH3O42WoBpzvWMZ5H5OC','2016-03-04 15:19:51', '2016-03-04 15:19:51'),
	(11, 'Khor Lee Ling', '6598336785', 0, 'khorleeling@gmail.com', '$2y$10$0WPK3anB8vV2r3A6AhCRL.sip9hZ71WB986TQB3l5D9F4E.9ylMmC', '2016-03-04 15:22:23', '2016-03-04 15:22:23'),
	(12, 'Janice Jaiaraman ', '6590921130', 0, 'jjsd2402t01@gmail.com', '$2y$10$/Kxy6Z88Y1i/zCaDxiEFpOz0uYA3KmjJoU9V1kwCllVO7AL1MZtM2', '2016-03-04 15:29:27', '2016-03-04 15:31:45'),	
	(14, 'Koh Woan Ling', '6581827809', 0, 'lynnkwl2001@yahoo.com.sg', '$2y$10$ZqoVZiMVqYa4b.0Lv39zluFsGoxV4m.6aHgj8lal3/Tx2fiSMFvYm', '2016-03-04 15:41:01', '2016-03-04 15:41:01'),
	(15, 'cynthia Lim Leng Leng', '6597955919', 0, 'limcyn@gmail.com', '$2y$10$dQ5axoLooUB1nXJwqA/eQeAuXW2wXy1oWIN1C4kozCI/yFYdnR08K', '2016-03-04 15:47:12', '2016-03-04 15:47:12'),
	(16, 'Cindy Eng Kah Kah', '6584888789', 0, 'clairecin@yahoo.com', '$2y$10$X48PUPb.pPBivOYfHZWz4Oz3RaeZyJP7TiLrx/tPzmKsY5K6DCIk2', '2016-03-04 15:52:39', '2016-03-04 15:52:39'),
	(17, 'Esther Tang Shen Jiun', '6594235193', 0, 'icampusadmin@gmail.com', '$2y$10$PPG34dNkYLeB4dvioRdtvux5uRSjxHnV0iYWtpWEzRCzSnnmXThi6', '2016-03-04 15:54:35', '2016-03-26 17:35:37'),
	(18, 'Lor Junyuan', '6598434899', 0, 'loryuanyuan@gmail.com', '$2y$10$lwK06PM22czTB./6onGa5eJ/JrLVvZ64MLYl9d3keSZ2uXWz.RIRW', '2016-03-04 15:55:42', '2016-03-04 15:55:42'),
	(19, 'Wong Sheau Fang', '6597705428', 0, 'bee880122@yahoo.com', '$2y$10$yLNI2EJi2X8KMLBH87nyBukmgh67fMr7PAWXuIWbaaAKhZRlvXbhy', '2016-03-04 15:56:56', '2016-03-04 15:56:56'),
	(20, 'Rinnie Lee Siew Mui', '6596640563', 0, 'mycsembawang@gmail.com', '$2y$10$qshFTDFUwvqjEUwfl1VYReekaJ4tagJOvOzyyEnSJCLKZgfU62si2', '2016-03-04 16:01:08', '2016-03-04 16:01:44'),
	(23, 'myctaly@yahoo.com.sg ', '98462420', 0, 'myctaly@yahoo.com.sg', '$2y$10$ukkwCuguBF36A6JHNZfbwuGeyu0DKyipjmhk4Ocq/kr3GDZE6u/FO', '2016-03-25 14:56:52', '2016-05-19 14:32:39'),
	(24, 'Taly Yap Ling Ling', null, 0, 'myctaly@yahoo.com', '$2y$10$8wxIwJzXe9mQsH9BBx1Cw.qJ2q5u4byl5gCeKHtuk9m9TT74n6M2u', '2016-04-25 12:02:47', '2016-04-25 12:02:47');

SELECT setval('users_id_seq', (SELECT max(id) FROM users));