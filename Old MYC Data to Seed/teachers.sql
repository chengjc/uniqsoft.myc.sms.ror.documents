INSERT INTO teachers(
            id, gender, date_of_birth, number_of_children, full_time_occupation, 
            highest_piano_practical_standard_passed, highest_theory_standard_passed, 
            has_college_abrsm, has_college_trinity, has_college_lcm, college_others, 
            other_music_qualifications, before_myc_teaching_individual_lesson_started_year, 
            before_myc_teaching_individual_lesson_total_year, before_myc_teaching_group_lesson_started_year, 
            before_myc_teaching_group_lesson_total_year, date_of_myc_teacher_agreement, 
            date_of_myc_studio_agreement, date_of_myc_l1, date_of_myc_l2ss2, 
            date_of_myc_l2sb2, date_of_myc_l2mb2, date_of_myc_l3sb3, date_of_myc_l3mb3, 
            date_of_1st_teacher_visit, date_of_2nd_teacher_visit, date_of_3rd_teacher_visit, 
            user_id, created_at, updated_at)
    VALUES 
      (3, 'F', '1990-10-03', null, null, null, null, false, false, false, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 10, '2016-06-21', '2016-06-21'),
      (4, 'F', '1971-01-20', null, null, null, null, false, false, false, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 11, '2016-06-21', '2016-06-21'),
      (5, 'F', '1973-04-28', null, null, null, null, false, false, false, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 12, '2016-06-21', '2016-06-21'),
      (7, 'F', '1963-09-15', null, null, null, null, false, false, false, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 14, '2016-06-21', '2016-06-21'),
      (8, 'F', '1976-12-30', null, null, null, null, false, false, false, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 15, '2016-06-21', '2016-06-21'),
      (9, 'F', '1965-01-01', null, null, null, null, false, false, false, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 16, '2016-06-21', '2016-06-21'),
      (10, 'F', '1974-04-02', null, null, null, null, false, false, false, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 17, '2016-06-21', '2016-06-21'),
      (11, 'F', '1988-12-31', null, null, null, null, false, false, false, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 18, '2016-06-21', '2016-06-21'),
      (12, 'F', '1988-01-22', null, null, null, null, false, false, false, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 19, '2016-06-21', '2016-06-21'),
      (13, 'F', '1976-11-27', null, null, null, null, false, false, false, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 24, '2016-06-21', '2016-06-21');

SELECT setval('teachers_id_seq', (SELECT max(id) FROM teachers));