INSERT INTO latest_invoice_numbers(
            studio_id, latest_invoice_number, created_at, updated_at)
    VALUES (6, 1112, '2016-06-23', '2016-06-23'),
    (9, 1191, '2016-06-23', '2016-06-23'),
    (4, 1001, '2016-06-23', '2016-06-23');

SELECT setval('latest_invoice_numbers_id_seq', (SELECT max(id) FROM latest_invoice_numbers));	